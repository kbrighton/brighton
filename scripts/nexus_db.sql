create database nexus
    with owner postgres;

create table if not exists larps
(
    id         serial      not null
        constraint larps_pk
            primary key,
    name       varchar(30) not null,
    convention varchar(30),
    created_at time,
    updated_at time,
    deleted_at time
);

alter table larps
    owner to postgres;

create index if not exists idx_larps_deleted_at
    on larps (deleted_at);

create table if not exists users
(
    id         serial      not null
        constraint users_pk
            primary key,
    email      varchar(30) not null,
    created_at time,
    updated_at time,
    deleted_at time
);

alter table users
    owner to postgres;

create unique index if not exists users_email_uindex
    on users (email);

create index if not exists idx_users_deleted_at
    on users (deleted_at);

create table if not exists roles
(
    id         serial      not null
        constraint roles_pk
            primary key,
    role       varchar(30) not null,
    created_at time,
    updated_at time,
    deleted_at time
);

alter table roles
    owner to postgres;

create unique index if not exists roles_role_uindex
    on roles (role);

create table if not exists user_roles
(
    user_id integer not null
        constraint user_roles_users_id_fk
            references users,
    role_id integer not null
        constraint user_roles_roles_id_fk
            references roles
);

alter table user_roles
    owner to postgres;

create table if not exists series
(
    id         serial       not null
        constraint series_pk
            primary key,
    name       varchar(120) not null,
    created_at time,
    deleted_at time,
    updated_at time
);

alter table series
    owner to postgres;

create table if not exists mediums
(
    id         serial      not null
        constraint mediums_pk
            primary key,
    medium     varchar(25) not null,
    updated_at time,
    deleted_at time,
    created_at time
);

alter table mediums
    owner to postgres;

create table if not exists series_mediums
(
    series_id integer
        constraint series_mediums_series_id_fk
            references series,
    medium_id integer
        constraint series_mediums_mediums_id_fk
            references mediums
);

alter table series_mediums
    owner to postgres;

create table if not exists types
(
    id   serial      not null
        constraint types_pk
            primary key,
    type varchar(25) not null
);

alter table types
    owner to postgres;

create table if not exists image
(
    id           serial      not null
        constraint image_pk
            primary key,
    filelocation varchar(50) not null,
    scale        double precision,
    translatex   integer,
    translatey   integer
);

alter table image
    owner to postgres;

create table if not exists characters
(
    id         serial      not null
        constraint characters_pk
            primary key,
    name       varchar(25) not null,
    bio        text,
    image1     integer
        constraint characters_image1_fk
            references image,
    image2     integer
        constraint characters_image2_fk
            references image,
    created_at time,
    deleted_at time,
    updated_at time,
    series_id  integer
        constraint characters_series_id_fk
            references series
);

alter table characters
    owner to postgres;

create table if not exists powers
(
    id           serial not null
        constraint powers_pk
            primary key,
    name         text   not null,
    skill        varchar(15),
    cost         integer,
    description  text   not null,
    based_on     integer,
    created_at   time,
    deleted_at   time,
    updated_at   time,
    character_id integer
        constraint powers_characters_id_fk
            references characters,
    is_latest    boolean default false
);

alter table powers
    owner to postgres;

create table if not exists power_types
(
    power_id integer
        constraint power_types_powers_id_fk
            references powers,
    type_id  integer
        constraint power_types_types_id_fk
            references types
);

alter table power_types
    owner to postgres;

create table if not exists stats
(
    id                  serial  not null
        constraint stats_pk
            primary key,
    power               integer default 1,
    power_alt_name      varchar(15),
    athletics           integer default 1,
    athletics_alt_name  varchar(15),
    brains              integer default 1,
    brains_alt_name     varchar(15),
    confidence          integer default 1,
    confidence_alt_name varchar(15),
    charisma            integer default 1,
    charisma_alt_name   varchar(15),
    affluence           integer default 0,
    affluence_alt_name  varchar(15),
    character_id        integer
        constraint stats_characters_id_fk
            references characters,
    body                integer not null,
    energy              integer not null,
    resilience          integer,
    is_latest           boolean default false
);

alter table stats
    owner to postgres;

create table if not exists skills
(
    id              serial not null
        constraint skills_pk
            primary key,
    character_id    integer
        constraint skills_characters_id_fk
            references characters,
    acting          integer,
    computer_use    integer,
    deception       integer,
    influence       integer,
    magic_chi       integer,
    medicine        integer,
    pilot_vehicle   integer,
    science         integer,
    yoink           integer,
    authority       integer,
    hide_stuff      integer,
    disguise        integer,
    jump            integer,
    mechanics       integer,
    pilot_mecha     integer,
    run             integer,
    search          integer,
    stealth         integer,
    archery         integer,
    bfg             integer,
    blades          integer,
    blunt           integer,
    dodge           integer,
    hand_to_hand    integer,
    marksmanship    integer,
    mecha_weaponry  integer,
    special_weapons integer,
    throw           integer,
    authority_type  varchar(50),
    is_latest       boolean default false
);

alter table skills
    owner to postgres;

create table if not exists larp_mediums
(
    larp_id   integer
        constraint larp_mediums_larps_id_fk
            references larps,
    medium_id integer
        constraint larp_mediums_mediums_id_fk
            references mediums
);

alter table larp_mediums
    owner to postgres;

create table if not exists user_larps
(
    larp_id integer
        constraint user_larps_larps_id_fk
            references larps,
    user_id integer
        constraint user_larps_users_id_fk
            references users
);

alter table user_larps
    owner to postgres;

create table if not exists companion
(
    id   serial not null
        constraint companion_pk
            primary key,
    type varchar(25)
);

alter table companion
    owner to postgres;

create table if not exists items
(
    id               serial                not null
        constraint items_pk
            primary key,
    name             varchar(17)           not null,
    description      text                  not null,
    series_id        integer
        constraint items_series_id_fk
            references series,
    larp_id          integer
        constraint items_larps_id_fk
            references larps,
    two_sided        boolean default false not null,
    back_description text,
    is_sheet_item    boolean default false not null,
    integrity        integer,
    holdout          integer,
    uses             integer,
    image            integer
        constraint items_image_fk
            references image,
    is_companion     boolean default false,
    companion_id     integer
        constraint items_companion_id_fk
            references companion
);

alter table items
    owner to postgres;

create table if not exists items_characters
(
    item_id      integer
        constraint items_characters_items_id_fk
            references items,
    character_id integer
        constraint items_characters_characters_id_fk
            references characters
);

alter table items_characters
    owner to postgres;

