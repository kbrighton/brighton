###Introduction

Brighton strives to be a full featured character database and creation tool for use with the Nexus System family of LARPs, which run throughout the east coast of the United States.

###Features, both planned and implemented

- oAuth2 based authentication using auth0
- LARP Pre-registration and character sheet collation
- Sheet search
- Sheet editing and versioning
- Tagging system for both powers and series

We use auth0 for our authentication

[![Single Sign On & Token Based Authentication - Auth0](http://cdn.auth0.com/oss/badges/a0-badge-light.png)](https://auth0.com/?utm_source=oss&utm_medium=gp&utm_campaign=oss)

