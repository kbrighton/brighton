package main

import (
	"bitbucket.org/kbrighton/brighton/pkg/auth"
	"bitbucket.org/kbrighton/brighton/pkg/characters"
	"bitbucket.org/kbrighton/brighton/pkg/database"
	"bitbucket.org/kbrighton/brighton/pkg/larps"
	"bitbucket.org/kbrighton/brighton/pkg/mediums"
	"bitbucket.org/kbrighton/brighton/pkg/series"
	"fmt"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/contrib/static"
	"github.com/gin-gonic/gin"
	"github.com/go-pg/pg"
	"github.com/tkanos/gonfig"
	"log"
	"net/http"
	"time"
)

type Configuration struct {
	Password string
	User     string
	Address  string
	Database string
}

func main() {

	// TODO: Get logs also in files -> use gin.DefaultWriter and io.MultiWriter
	// TODO: Lets get some more configurations

	/* Routes */

	configuration := Configuration{}
	err := gonfig.GetConf("config.json", &configuration)
	if err != nil {
		fmt.Println(err)
	}

	database.Manager = pg.Connect(&pg.Options{
		Addr:     configuration.Address,
		User:     configuration.User,
		Password: configuration.Password,
		Database: configuration.Database,
	})

	// Set the router as the default one shipped with Gin
	router := gin.Default()

	// Cors
	router.Use(cors.New(cors.Config{
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{"PUT", "PATCH", "DELETE", "GET", "POST"},
		AllowHeaders:     []string{"Origin", "Authorization", "Content-Type"},
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
		MaxAge:           12 * time.Hour,
	}))

	// Serve frontend static files
	router.Use(static.Serve("/", static.LocalFile("./views", true)))

	// Setup route group for the API
	api := router.Group("/api/v1")
	{
		api.GET("/larps", auth.Auth0Groups("read:characters"), larps.GetLarps)
		api.GET("/mediums", auth.Auth0Groups("read:characters"), mediums.GetMediums)
		api.GET("/series", auth.Auth0Groups("read:characters"), series.GetSeries)
		api.GET("/characters/seriesid/:seriesid", auth.Auth0Groups("read:characters"), characters.GetCharactersBySeries)
		api.PUT("/characters", auth.Auth0Groups("read:characters"), characters.PostCharacter)
	}

	noapi := router.Group("/noapi")
	{
		noapi.GET("/", func(c *gin.Context) {
			log.Println(c.Request.URL)
			c.JSON(http.StatusOK, gin.H{
				"message": "pong",
			})
		})

	}

	// Start and run the server
	router.Run(":3000")

}
