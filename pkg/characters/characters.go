package characters

import (
	"bitbucket.org/kbrighton/brighton/pkg/database"
	"bitbucket.org/kbrighton/brighton/pkg/model"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
)

func GetCharactersBySeries(c *gin.Context) {
	var characters []model.Character

	seriesId := c.Param("seriesid")

	db := database.Manager

	db.Model(&characters).Relation("Series").Where("series.id = ?", seriesId).Select()

	log.Println(characters)

	c.JSON(http.StatusOK, gin.H{
		"characters": characters,
	})
	return
}

func PostCharacter(c *gin.Context) {

	//db := database.Manager

	var character model.Character

	c.BindJSON(&character)

	log.Println(character)

}
