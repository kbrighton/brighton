package model

import (
	"github.com/go-pg/pg"
)

type Power struct {
	tableName struct{} `sql:"powers,alias:t" pg:",discard_unknown_columns"`

	ID          int         `sql:"id,pk"`
	BasedOn     *int        `sql:"based_on"`
	CharacterID *int        `sql:"character_id"`
	Cost        *int        `sql:"cost"`
	CreatedAt   pg.NullTime `sql:"created_at"`
	DeletedAt   pg.NullTime `sql:"deleted_at"`
	Description string      `sql:"description,notnull"`
	IsLatest    *bool       `sql:"is_latest"`
	Name        string      `sql:"name,notnull"`
	Skill       *string     `sql:"skill"`
	UpdatedAt   pg.NullTime `sql:"updated_at"`

	Character *Character `pg:"fk:character_id"`
}
