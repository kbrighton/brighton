package model

type Stat struct {
	tableName struct{} `sql:"stats,alias:t" pg:",discard_unknown_columns"`

	ID                int     `sql:"id,pk"`
	Affluence         *int    `sql:"affluence"`
	AffluenceAltName  *string `sql:"affluence_alt_name"`
	Athletics         *int    `sql:"athletics"`
	AthleticsAltName  *string `sql:"athletics_alt_name"`
	Body              int     `sql:"body,notnull"`
	Brains            *int    `sql:"brains"`
	BrainsAltName     *string `sql:"brains_alt_name"`
	CharacterID       *int    `sql:"character_id"`
	Charisma          *int    `sql:"charisma"`
	CharismaAltName   *string `sql:"charisma_alt_name"`
	Confidence        *int    `sql:"confidence"`
	ConfidenceAltName *string `sql:"confidence_alt_name"`
	Energy            int     `sql:"energy,notnull"`
	IsLatest          *bool   `sql:"is_latest"`
	Power             *int    `sql:"power"`
	PowerAltName      *string `sql:"power_alt_name"`
	Resilience        *int    `sql:"resilience"`

	Character *Character `pg:"fk:character_id"`
}
