package model

type Type struct {
	tableName struct{} `sql:"types,alias:t" pg:",discard_unknown_columns"`

	ID   int    `sql:"id,pk"`
	Type string `sql:"type,notnull"`
}
