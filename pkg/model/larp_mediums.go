package model

type LarpMedium struct {
	tableName struct{} `sql:"larp_mediums,alias:t" pg:",discard_unknown_columns"`

	LarpID   *int `sql:"larp_id"`
	MediumID *int `sql:"medium_id"`

	Larp   *Larp   `pg:"fk:larp_id"`
	Medium *Medium `pg:"fk:medium_id"`
}
