package model

type Image struct {
	tableName struct{} `sql:"image,alias:t" pg:",discard_unknown_columns"`

	ID           int      `sql:"id,pk"`
	Filelocation string   `sql:"filelocation,notnull"`
	Scale        *float64 `sql:"scale"`
	Translatex   *int     `sql:"translatex"`
	Translatey   *int     `sql:"translatey"`
}
