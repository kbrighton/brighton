package model

import (
	"github.com/go-pg/pg"
)

type Series struct {
	tableName struct{} `sql:"series,alias:t" pg:",discard_unknown_columns"`

	ID         int         `sql:"id,pk"`
	CreatedAt  pg.NullTime `sql:"created_at"`
	DeletedAt  pg.NullTime `sql:"deleted_at"`
	Name       string      `sql:"name,notnull"`
	UpdatedAt  pg.NullTime `sql:"updated_at"`
	Mediums    []Medium    `pg:"many2many:series_mediums"`
	Characters []*Character
}
