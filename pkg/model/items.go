package model

type Item struct {
	tableName struct{} `sql:"items,alias:t" pg:",discard_unknown_columns"`

	ID              int     `sql:"id,pk"`
	BackDescription *string `sql:"back_description"`
	CharacterID     *int    `sql:"character_id"`
	CompanionID     *int    `sql:"companion_id"`
	Description     string  `sql:"description,notnull"`
	Holdout         *int    `sql:"holdout"`
	Image           *int    `sql:"image"`
	Integrity       *int    `sql:"integrity"`
	IsCompanion     *bool   `sql:"is_companion"`
	IsSheetItem     bool    `sql:"is_sheet_item,notnull"`
	LarpID          *int    `sql:"larp_id"`
	Name            string  `sql:"name,notnull"`
	SeriesID        *int    `sql:"series_id"`
	TwoSided        bool    `sql:"two_sided,notnull"`
	Uses            *int    `sql:"uses"`

	Character *Character `pg:"fk:character_id"`
	Companion *Companion `pg:"fk:companion_id"`
	ImageRel  *Image     `pg:"fk:image"`
	Larp      *Larp      `pg:"fk:larp_id"`
	Series    *Series    `pg:"fk:series_id"`
}
