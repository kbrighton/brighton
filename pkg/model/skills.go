package model

type Skill struct {
	tableName struct{} `sql:"skills,alias:t" pg:",discard_unknown_columns"`

	ID             int     `sql:"id,pk"`
	Acting         *int    `sql:"acting"`
	Archery        *int    `sql:"archery"`
	Authority      *int    `sql:"authority"`
	AuthorityType  *string `sql:"authority_type"`
	Bfg            *int    `sql:"bfg"`
	Blades         *int    `sql:"blades"`
	Blunt          *int    `sql:"blunt"`
	CharacterID    *int    `sql:"character_id"`
	ComputerUse    *int    `sql:"computer_use"`
	Deception      *int    `sql:"deception"`
	Disguise       *int    `sql:"disguise"`
	Dodge          *int    `sql:"dodge"`
	HandToHand     *int    `sql:"hand_to_hand"`
	HideStuff      *int    `sql:"hide_stuff"`
	Influence      *int    `sql:"influence"`
	IsLatest       *bool   `sql:"is_latest"`
	Jump           *int    `sql:"jump"`
	MagicChi       *int    `sql:"magic_chi"`
	Marksmanship   *int    `sql:"marksmanship"`
	MechaWeaponry  *int    `sql:"mecha_weaponry"`
	Mechanics      *int    `sql:"mechanics"`
	Medicine       *int    `sql:"medicine"`
	PilotMecha     *int    `sql:"pilot_mecha"`
	PilotVehicle   *int    `sql:"pilot_vehicle"`
	Run            *int    `sql:"run"`
	Science        *int    `sql:"science"`
	Search         *int    `sql:"search"`
	SpecialWeapons *int    `sql:"special_weapons"`
	Stealth        *int    `sql:"stealth"`
	Throw          *int    `sql:"throw"`
	Yoink          *int    `sql:"yoink"`

	Character *Character `pg:"fk:character_id"`
}
