package model

import (
	"github.com/go-pg/pg"
)

type Character struct {
	tableName struct{} `sql:"characters,alias:t" pg:",discard_unknown_columns"`

	ID        int         `sql:"id,pk"`
	Bio       *string     `sql:"bio"`
	CreatedAt pg.NullTime `sql:"created_at"`
	DeletedAt pg.NullTime `sql:"deleted_at"`
	Image1    *int        `sql:"image1"`
	Image2    *int        `sql:"image2"`
	Name      string      `sql:"name,notnull"`
	SeriesID  *int        `sql:"series_id"`
	UpdatedAt pg.NullTime `sql:"updated_at"`
	Stats     []*Stat
	Skills    []*Skill
	Powers    []*Power
	Items     *Item

	Image1Rel *Image  `pg:"fk:image1"`
	Image2Rel *Image  `pg:"fk:image2"`
	Series    *Series `pg:"fk:series_id"`
}
