package model

import (
	"github.com/go-pg/pg"
)

type Role struct {
	tableName struct{} `sql:"roles,alias:t" pg:",discard_unknown_columns"`

	ID        int         `sql:"id,pk"`
	CreatedAt pg.NullTime `sql:"created_at"`
	DeletedAt pg.NullTime `sql:"deleted_at"`
	Role      string      `sql:"role,notnull"`
	UpdatedAt pg.NullTime `sql:"updated_at"`
}
