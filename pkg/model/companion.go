package model

type Companion struct {
	tableName struct{} `sql:"companion,alias:t" pg:",discard_unknown_columns"`

	ID   int     `sql:"id,pk"`
	Type *string `sql:"type"`
}
