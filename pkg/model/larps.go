package model

import (
	"github.com/go-pg/pg"
)

type Larp struct {
	tableName struct{} `sql:"larps,alias:t" pg:",discard_unknown_columns"`

	ID         int         `sql:"id,pk"`
	Convention string      `sql:"convention"`
	CreatedAt  pg.NullTime `sql:"created_at"`
	DeletedAt  pg.NullTime `sql:"deleted_at"`
	Name       string      `sql:"name,notnull"`
	UpdatedAt  pg.NullTime `sql:"updated_at"`
	Mediums    []Medium    `pg:"many2many:larp_mediums"`
	Users      []Larp      `pg:"many2many:user_larps"`
}
