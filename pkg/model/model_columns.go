package model

var Columns = struct {
	User struct {
		ID, CreatedAt, DeletedAt, Email, UpdatedAt string
	}
	Larp struct {
		ID, Convention, CreatedAt, DeletedAt, Name, UpdatedAt string
	}
	Medium struct {
		ID, CreatedAt, DeletedAt, Medium, UpdatedAt string
	}
	PowerType struct {
		PowerID, TypeID string

		Power, Type string
	}
	Series struct {
		ID, CreatedAt, DeletedAt, MediumID, Name, UpdatedAt string
	}
	SeriesMedium struct {
		MediumID, SeriesID string

		Medium, Series string
	}
	Character struct {
		ID, Bio, CreatedAt, DeletedAt, Image1, Image2, Name, SeriesID, UpdatedAt string

		Image1Rel, Image2Rel, Series string
	}
	Companion struct {
		ID, Type string
	}
	Image struct {
		ID, Filelocation, Scale, Translatex, Translatey string
	}
	Role struct {
		ID, CreatedAt, DeletedAt, Role, UpdatedAt string
	}
	Skill struct {
		ID, Acting, Archery, Authority, AuthorityType, Bfg, Blades, Blunt, CharacterID, ComputerUse, Deception, Disguise, Dodge, HandToHand, HideStuff, Influence, IsLatest, Jump, MagicChi, Marksmanship, MechaWeaponry, Mechanics, Medicine, PilotMecha, PilotVehicle, Run, Science, Search, SpecialWeapons, Stealth, Throw, Yoink string

		Character string
	}
	Type struct {
		ID, Type string
	}
	UserRole struct {
		RoleID, UserID string

		Role, User string
	}
	Item struct {
		ID, BackDescription, CharacterID, CompanionID, Description, Holdout, Image, Integrity, IsCompanion, IsSheetItem, LarpID, Name, SeriesID, TwoSided, Uses string

		Character, Companion, ImageRel, Larp, Series string
	}
	LarpMedium struct {
		LarpID, MediumID string

		Larp, Medium string
	}
	Power struct {
		ID, BasedOn, CharacterID, Cost, CreatedAt, DeletedAt, Description, IsLatest, Name, Skill, UpdatedAt string

		Character string
	}
	Stat struct {
		ID, Affluence, AffluenceAltName, Athletics, AthleticsAltName, Body, Brains, BrainsAltName, CharacterID, Charisma, CharismaAltName, Confidence, ConfidenceAltName, Energy, IsLatest, Power, PowerAltName, Resilience string

		Character string
	}
	UserLarp struct {
		LarpsID, UsersID string

		Larps, Users string
	}
}{
	User: struct {
		ID, CreatedAt, DeletedAt, Email, UpdatedAt string
	}{
		ID:        "id",
		CreatedAt: "created_at",
		DeletedAt: "deleted_at",
		Email:     "email",
		UpdatedAt: "updated_at",
	},
	Larp: struct {
		ID, Convention, CreatedAt, DeletedAt, Name, UpdatedAt string
	}{
		ID:         "id",
		Convention: "convention",
		CreatedAt:  "created_at",
		DeletedAt:  "deleted_at",
		Name:       "name",
		UpdatedAt:  "updated_at",
	},
	Medium: struct {
		ID, CreatedAt, DeletedAt, Medium, UpdatedAt string
	}{
		ID:        "id",
		CreatedAt: "created_at",
		DeletedAt: "deleted_at",
		Medium:    "medium",
		UpdatedAt: "updated_at",
	},
	PowerType: struct {
		PowerID, TypeID string

		Power, Type string
	}{
		PowerID: "power_id",
		TypeID:  "type_id",

		Power: "Power",
		Type:  "Type",
	},
	Series: struct {
		ID, CreatedAt, DeletedAt, MediumID, Name, UpdatedAt string
	}{
		ID:        "id",
		CreatedAt: "created_at",
		DeletedAt: "deleted_at",
		MediumID:  "medium_id",
		Name:      "name",
		UpdatedAt: "updated_at",
	},
	SeriesMedium: struct {
		MediumID, SeriesID string

		Medium, Series string
	}{
		MediumID: "medium_id",
		SeriesID: "series_id",

		Medium: "Medium",
		Series: "Series",
	},
	Character: struct {
		ID, Bio, CreatedAt, DeletedAt, Image1, Image2, Name, SeriesID, UpdatedAt string

		Image1Rel, Image2Rel, Series string
	}{
		ID:        "id",
		Bio:       "bio",
		CreatedAt: "created_at",
		DeletedAt: "deleted_at",
		Image1:    "image1",
		Image2:    "image2",
		Name:      "name",
		SeriesID:  "series_id",
		UpdatedAt: "updated_at",

		Image1Rel: "Image1Rel",
		Image2Rel: "Image2Rel",
		Series:    "Series",
	},
	Companion: struct {
		ID, Type string
	}{
		ID:   "id",
		Type: "type",
	},
	Image: struct {
		ID, Filelocation, Scale, Translatex, Translatey string
	}{
		ID:           "id",
		Filelocation: "filelocation",
		Scale:        "scale",
		Translatex:   "translatex",
		Translatey:   "translatey",
	},
	Role: struct {
		ID, CreatedAt, DeletedAt, Role, UpdatedAt string
	}{
		ID:        "id",
		CreatedAt: "created_at",
		DeletedAt: "deleted_at",
		Role:      "role",
		UpdatedAt: "updated_at",
	},
	Skill: struct {
		ID, Acting, Archery, Authority, AuthorityType, Bfg, Blades, Blunt, CharacterID, ComputerUse, Deception, Disguise, Dodge, HandToHand, HideStuff, Influence, IsLatest, Jump, MagicChi, Marksmanship, MechaWeaponry, Mechanics, Medicine, PilotMecha, PilotVehicle, Run, Science, Search, SpecialWeapons, Stealth, Throw, Yoink string

		Character string
	}{
		ID:             "id",
		Acting:         "acting",
		Archery:        "archery",
		Authority:      "authority",
		AuthorityType:  "authority_type",
		Bfg:            "bfg",
		Blades:         "blades",
		Blunt:          "blunt",
		CharacterID:    "character_id",
		ComputerUse:    "computer_use",
		Deception:      "deception",
		Disguise:       "disguise",
		Dodge:          "dodge",
		HandToHand:     "hand_to_hand",
		HideStuff:      "hide_stuff",
		Influence:      "influence",
		IsLatest:       "is_latest",
		Jump:           "jump",
		MagicChi:       "magic_chi",
		Marksmanship:   "marksmanship",
		MechaWeaponry:  "mecha_weaponry",
		Mechanics:      "mechanics",
		Medicine:       "medicine",
		PilotMecha:     "pilot_mecha",
		PilotVehicle:   "pilot_vehicle",
		Run:            "run",
		Science:        "science",
		Search:         "search",
		SpecialWeapons: "special_weapons",
		Stealth:        "stealth",
		Throw:          "throw",
		Yoink:          "yoink",

		Character: "Character",
	},
	Type: struct {
		ID, Type string
	}{
		ID:   "id",
		Type: "type",
	},
	UserRole: struct {
		RoleID, UserID string

		Role, User string
	}{
		RoleID: "role_id",
		UserID: "user_id",

		Role: "Role",
		User: "User",
	},
	Item: struct {
		ID, BackDescription, CharacterID, CompanionID, Description, Holdout, Image, Integrity, IsCompanion, IsSheetItem, LarpID, Name, SeriesID, TwoSided, Uses string

		Character, Companion, ImageRel, Larp, Series string
	}{
		ID:              "id",
		BackDescription: "back_description",
		CharacterID:     "character_id",
		CompanionID:     "companion_id",
		Description:     "description",
		Holdout:         "holdout",
		Image:           "image",
		Integrity:       "integrity",
		IsCompanion:     "is_companion",
		IsSheetItem:     "is_sheet_item",
		LarpID:          "larp_id",
		Name:            "name",
		SeriesID:        "series_id",
		TwoSided:        "two_sided",
		Uses:            "uses",

		Character: "Character",
		Companion: "Companion",
		ImageRel:  "ImageRel",
		Larp:      "Larp",
		Series:    "Series",
	},
	LarpMedium: struct {
		LarpID, MediumID string

		Larp, Medium string
	}{
		LarpID:   "larp_id",
		MediumID: "medium_id",

		Larp:   "Larp",
		Medium: "Medium",
	},
	Power: struct {
		ID, BasedOn, CharacterID, Cost, CreatedAt, DeletedAt, Description, IsLatest, Name, Skill, UpdatedAt string

		Character string
	}{
		ID:          "id",
		BasedOn:     "based_on",
		CharacterID: "character_id",
		Cost:        "cost",
		CreatedAt:   "created_at",
		DeletedAt:   "deleted_at",
		Description: "description",
		IsLatest:    "is_latest",
		Name:        "name",
		Skill:       "skill",
		UpdatedAt:   "updated_at",

		Character: "Character",
	},
	Stat: struct {
		ID, Affluence, AffluenceAltName, Athletics, AthleticsAltName, Body, Brains, BrainsAltName, CharacterID, Charisma, CharismaAltName, Confidence, ConfidenceAltName, Energy, IsLatest, Power, PowerAltName, Resilience string

		Character string
	}{
		ID:                "id",
		Affluence:         "affluence",
		AffluenceAltName:  "affluence_alt_name",
		Athletics:         "athletics",
		AthleticsAltName:  "athletics_alt_name",
		Body:              "body",
		Brains:            "brains",
		BrainsAltName:     "brains_alt_name",
		CharacterID:       "character_id",
		Charisma:          "charisma",
		CharismaAltName:   "charisma_alt_name",
		Confidence:        "confidence",
		ConfidenceAltName: "confidence_alt_name",
		Energy:            "energy",
		IsLatest:          "is_latest",
		Power:             "power",
		PowerAltName:      "power_alt_name",
		Resilience:        "resilience",

		Character: "Character",
	},
	UserLarp: struct {
		LarpsID, UsersID string

		Larps, Users string
	}{
		LarpsID: "larps_id",
		UsersID: "users_id",

		Larps: "Larps",
		Users: "Users",
	},
}

var Tables = struct {
	User struct {
		Name string
	}
	Larp struct {
		Name string
	}
	Medium struct {
		Name string
	}
	PowerType struct {
		Name string
	}
	Series struct {
		Name string
	}
	SeriesMedium struct {
		Name string
	}
	Character struct {
		Name string
	}
	Companion struct {
		Name string
	}
	Image struct {
		Name string
	}
	Role struct {
		Name string
	}
	Skill struct {
		Name string
	}
	Type struct {
		Name string
	}
	UserRole struct {
		Name string
	}
	Item struct {
		Name string
	}
	LarpMedium struct {
		Name string
	}
	Power struct {
		Name string
	}
	Stat struct {
		Name string
	}
	UserLarp struct {
		Name string
	}
}{
	User: struct {
		Name string
	}{
		Name: "users",
	},
	Larp: struct {
		Name string
	}{
		Name: "larps",
	},
	Medium: struct {
		Name string
	}{
		Name: "mediums",
	},
	PowerType: struct {
		Name string
	}{
		Name: "power_types",
	},
	Series: struct {
		Name string
	}{
		Name: "series",
	},
	SeriesMedium: struct {
		Name string
	}{
		Name: "series_mediums",
	},
	Character: struct {
		Name string
	}{
		Name: "characters",
	},
	Companion: struct {
		Name string
	}{
		Name: "companion",
	},
	Image: struct {
		Name string
	}{
		Name: "image",
	},
	Role: struct {
		Name string
	}{
		Name: "roles",
	},
	Skill: struct {
		Name string
	}{
		Name: "skills",
	},
	Type: struct {
		Name string
	}{
		Name: "types",
	},
	UserRole: struct {
		Name string
	}{
		Name: "user_roles",
	},
	Item: struct {
		Name string
	}{
		Name: "items",
	},
	LarpMedium: struct {
		Name string
	}{
		Name: "larp_mediums",
	},
	Power: struct {
		Name string
	}{
		Name: "powers",
	},
	Stat: struct {
		Name string
	}{
		Name: "stats",
	},
	UserLarp: struct {
		Name string
	}{
		Name: "user_larps",
	},
}
