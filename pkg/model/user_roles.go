package model

type UserRole struct {
	tableName struct{} `sql:"user_roles,alias:t" pg:",discard_unknown_columns"`

	RoleID int `sql:"role_id,notnull"`
	UserID int `sql:"user_id,notnull"`

	Role *Role `pg:"fk:role_id"`
	User *User `pg:"fk:user_id"`
}
