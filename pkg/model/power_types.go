package model

type PowerType struct {
	tableName struct{} `sql:"power_types,alias:t" pg:",discard_unknown_columns"`

	PowerID *int `sql:"power_id"`
	TypeID  *int `sql:"type_id"`

	Power *Power `pg:"fk:power_id"`
	Type  *Type  `pg:"fk:type_id"`
}
