package model

type SeriesMedium struct {
	tableName struct{} `sql:"series_mediums,alias:t" pg:",discard_unknown_columns"`

	MediumID *int `sql:"medium_id"`
	SeriesID *int `sql:"series_id"`

	Medium *Medium `pg:"fk:medium_id"`
	Series *Series `pg:"fk:series_id"`
}
