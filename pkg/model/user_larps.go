package model

type UserLarp struct {
	tableName struct{} `sql:"user_larps,alias:t" pg:",discard_unknown_columns"`

	LarpsID *int `sql:"larps_id"`
	UsersID *int `sql:"users_id"`

	Larps *Larp `pg:"fk:larps_id"`
	Users *User `pg:"fk:users_id"`
}
