package model

import (
	"github.com/go-pg/pg"
)

type User struct {
	tableName struct{} `sql:"users,alias:t" pg:",discard_unknown_columns"`

	ID        int         `sql:"id,pk"`
	CreatedAt pg.NullTime `sql:"created_at"`
	DeletedAt pg.NullTime `sql:"deleted_at"`
	Email     string      `sql:"email,notnull"`
	UpdatedAt pg.NullTime `sql:"updated_at"`
	Larps     []Larp      `pg:"many2many:user_larps"`
	Roles     []Role      `pg:"many2many:user_roles"`
}
