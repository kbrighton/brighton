package model

import (
	"github.com/go-pg/pg"
)

type Medium struct {
	tableName struct{} `sql:"mediums,alias:t" pg:",discard_unknown_columns"`

	ID        int         `sql:"id,pk"`
	CreatedAt pg.NullTime `sql:"created_at"`
	DeletedAt pg.NullTime `sql:"deleted_at"`
	Medium    string      `sql:"medium,notnull"`
	UpdatedAt pg.NullTime `sql:"updated_at"`
	Larps     []Larp      `pg:"many2many:larp_mediums"`
	Series    []Series    `pg:"many2many:series_mediums"`
}
