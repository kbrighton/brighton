package larps

import (
	"bitbucket.org/kbrighton/brighton/pkg/database"
	"bitbucket.org/kbrighton/brighton/pkg/model"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
)

func GetLarps(c *gin.Context) {
	var larps []model.Larp

	db := database.Manager

	db.Model(&larps).Relation("Mediums").Select()

	log.Println(larps)

	c.JSON(http.StatusOK, gin.H{
		"larps": larps,
	})
	return
}
