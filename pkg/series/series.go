package series

import (
	"bitbucket.org/kbrighton/brighton/pkg/database"
	"bitbucket.org/kbrighton/brighton/pkg/model"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
)

func GetSeries(c *gin.Context) {
	var series []model.Series

	db := database.Manager

	db.Model(&series).Relation("Characters").Select()

	log.Println(series)

	c.JSON(http.StatusOK, gin.H{
		"series": series,
	})
	return
}
