package mediums

import (
	"bitbucket.org/kbrighton/brighton/pkg/database"
	"bitbucket.org/kbrighton/brighton/pkg/model"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
)

func GetMediums(c *gin.Context) {
	var mediums []model.Medium

	db := database.Manager

	db.Model(&mediums).Relation("Larps").Relation("Series").Select()

	log.Println(mediums)

	c.JSON(http.StatusOK, gin.H{
		"mediums": mediums,
	})
	return
}
